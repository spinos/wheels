ARG BASE
ARG RELEASE
FROM $BASE

ENV RELEASE=$RELEASE

COPY  *.whl /wheels/
COPY Dockerfile /
RUN find /wheels/ -name '*.whl' -exec basename {} \; > /wheels.txt


WORKDIR /
CMD ["/bin/sh"]
