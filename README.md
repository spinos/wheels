# Openstack Wheels for Alpine Linux

This is a data container image that contains wheels for the primary
Openstack projects and their dependencies. These wheels are built for
Alpine Linux, therefore are built against the musl c library.

## Build Instructions

Set BUILD environment variable

BUILD  must contain the build output from running the
`spinos/wheel-builder` container:

```
$HOME
  |
  +-build
    |
    +-spinos
      |
      +-<ALPINE_BASE_VERSION>
        |
        +-<OPENSTACK_RELEASE_NAME>
          |
          +-wheels
```

For example: $HOME/build/spinos/3.9/ocata/wheels

```
echo '$HOME/build' >> $HOME/.profile
```

Create container image

```
make image
```

Tag container image

```
make tag
```

Login to Docker Hub

```
docker login
```

Push container to Docker Hub

```
make push
```
