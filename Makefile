.PHONY: image tag push clean

#Docker org for this image
ORG?=spinos

#Docker repo name for this image
IMAGE=wheels

#Openstack release name
RELEASE=ocata

#Docker base image
BASENAME=alpine
BASEVER=3.9
BASE=$(BASENAME):$(BASEVER)

WHEELS=$(BUILD)/spinos/$(BASEVER)/$(RELEASE)/wheels

default: tag

iid: Makefile Dockerfile
	cp Dockerfile $(WHEELS)/
	docker build --build-arg BASE=$(BASE) --build-arg RELEASE=$(RELEASE) --iidfile iid --file Dockerfile $(WHEELS)
	rm $(WHEELS)/Dockerfile

hash: iid
	docker run --rm --entrypoint '' $(shell cat iid) /bin/sh -c 'echo wheels.txt | xargs cat | sha1sum' | sed 's/ .*//' > $@

image: hash

tag: image
	docker tag $(shell cat iid) $(ORG)/$(IMAGE):$(shell cat hash)
	docker tag $(shell cat iid) $(ORG)/$(IMAGE):$(RELEASE)
	docker tag $(shell cat iid) $(ORG)/$(IMAGE):latest

push: tag
	docker push $(ORG)/$(IMAGE):$(shell cat hash)
	docker push $(ORG)/$(IMAGE):$(RELEASE)
	docker push $(ORG)/$(IMAGE):latest

cli:
	docker run -it --rm $(ORG)/$(IMAGE):$(shell cat hash) /bin/sh

clean:
	rm -f hash iid
